'use strict';
const configs = require('./config/dconfig');
const { getVideoDurationInSeconds } = require('get-video-duration');
const extractFrames = require('ffmpeg-extract-frames')
const fs = require('fs');
const aws = require('aws-sdk');
aws.config.update({
    secretAccessKey:configs.awsConfig.secretAccessKey,
    accessKeyId: configs.awsConfig.accessKeyId,
    region: configs.awsConfig.region,
});
var s3 = new aws.S3();
var baseUrl=configs.awsConfig.baseUrl;
var bucketName=configs.awsConfig.bucketName;
var n=5;
// Upload file to aws s3 
function tempUpload(tempPath){
  let timestamp=Date.now();
  let res = tempPath.split("upload");
  let fpath=timestamp+res[1];
  return new Promise(function(resolve,reject){
       fs.readFile(tempPath, function(err, file_buffer){
           var params = {
               Bucket: configs.awsConfig.bucketName,
               Key: fpath,
               Body: file_buffer,
               ACL:'public-read'
           };
           s3.putObject(params, function (perr, pres) {
               if (perr) {
                   console.log("Error uploading data: ", perr);
               } 
               else {
                   fs.unlinkSync(tempPath)
                   resolve({
                       url:baseUrl+bucketName+"/"+fpath,
                   });
               }
           });
       });
  })
}
// api to upload video to s3 bucket.
module.exports.submitForm = async (event) => {
  try{
    let data=JSON.parse(event.body)
    let buff = new Buffer(data.url, 'base64');
    fs.writeFileSync('./video/temp.mp4', buff);
    let readVideoFolder = await fs.readdirSync("video");
    let path=__dirname+"/video/"+readVideoFolder[0];
    let duration=await getVideoDurationInSeconds(path);
    let tempDuration=(duration/n)*1000;
    let temp=0
    let arr=[];
    for(let i=0;i<n;i++){
      temp=temp+tempDuration;
      arr.push(temp)
    }
    await extractFrames({
      input: path,
      output: './upload/screenshot-%i.jpg',
      offsets: arr
    })
    let readFolderList = await fs.readdirSync("upload");
    let uploadUrlArray=[];
    for(const item of readFolderList){
      uploadUrlArray.push(await tempUpload('./upload/'+item))
    }
    fs.unlinkSync(path)
    return {
      statusCode: 200,
      body: JSON.stringify({
        message: 'Video to screenshot converted and successfully uploaded to s3 bucket.',
        data:uploadUrlArray
      }, null, 2),
    };
  }catch(e){
    return {
      statusCode: 400,
      body: JSON.stringify({
        message: e.message,
        data:{}
      }, null, 2),
    };
  }
};
 